/*
 * uTron
 * Copyright (C) 2019  Nicolò Santamaria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "utron/engine.h"
#include "utron/bot.h"


#define FBUCKET ".concepts"


struct bot {
	int64_t chat_id;
	_Bool add_mode;
};


struct bot *new_bot(int64_t chat_id) {
	struct bot *bot = malloc(sizeof(struct bot));

	bot->chat_id = chat_id;
	bot->add_mode = 0;
	return bot;
}


void insert_concept(const char *concept) {
	FILE *fp;
	fp = fopen(FBUCKET, "a");

	fputs(concept, fp);
	fputs("%0A%0A", fp);
	fclose(fp);
}


void list_concepts(int64_t chat_id) {
	FILE *fp;
	char *buffer;
	long fsize;

	fp = fopen(FBUCKET, "rb");

	if (fp != NULL) {
		fseek(fp, 0, SEEK_END);
		fsize = ftell(fp);
		rewind(fp);

		if (fsize > 0) {
			buffer = malloc(fsize);
			fread(buffer, fsize, 1, fp);

			tg_send_message(buffer, chat_id);
			free(buffer);
		}

		fclose(fp);
	}

	else
		tg_send_message("List is empty", chat_id);
}


void *update_bot(void *vargp) {
	struct bot_update_arg *arg = vargp;
	struct bot *bot = arg->bot_ptr;
	struct json_object *update = arg->update;
	struct json_object *messageobj, *fromobj, *usrobj, *textobj;

	if (json_object_object_get_ex(update, "message", &messageobj)
			&& json_object_object_get_ex(messageobj, "from", &fromobj)
			&& json_object_object_get_ex(messageobj, "text", &textobj)
			&& json_object_object_get_ex(fromobj, "username", &usrobj)) {

		
		const char *text = json_object_get_string(textobj);
		const char *username = json_object_get_string(usrobj);

		if (!strcmp(text, "/ping"))
			tg_send_message("Hey!", bot->chat_id);

		else if (!strcmp(text, "/add")) {
			bot->add_mode = 1;
			tg_send_message("Tell me your concept", bot->chat_id);
		}

		else if (!strcmp(text, "/dismiss")) {
			bot->add_mode = 0;
			tg_send_message("Add command dismissed", bot->chat_id);
		}

		else if (!strcmp(text, "/list"))
			list_concepts(bot->chat_id);

		else {
			if (bot->add_mode) {
				bot->add_mode = 0;
				insert_concept(text);
				tg_send_message("Concept added successfully", bot->chat_id);
			}
		}
	}
}
